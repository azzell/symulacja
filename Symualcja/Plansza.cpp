#include "stdafx.h"
#include "Plansza.h"
#include <windows.h>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Sterowanie.h"

using namespace std;
void Plansza::UstawieniaPoczatkowe()
{
	//ukrycie kursora
	::HANDLE hConsoleOut = ::GetStdHandle(STD_OUTPUT_HANDLE);
	::CONSOLE_CURSOR_INFO hCCI;
	::GetConsoleCursorInfo(hConsoleOut, &hCCI);
	hCCI.bVisible = FALSE;
	::SetConsoleCursorInfo(hConsoleOut, &hCCI);

	//zmiana koloru
	system("COLOR F0");

	//zmiana rozmiaru okna
	_COORD coord;
	coord.X = 100;
	coord.Y = 30;

	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = 30 - 1;
	Rect.Right = 100 - 1;

	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);      // Get Handle 
	SetConsoleScreenBufferSize(Handle, coord);            // Set Buffer Size 
	SetConsoleWindowInfo(Handle, TRUE, &Rect);            // Set Window Size

}

void Plansza::TworzP()
{
	COORD c;
	int j = 0, i = 0;

	c.X = 0;
	c.Y = 0;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << char(201);

	c.X = 99;
	c.Y = 0;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << char(187);

	c.X = 0;
	c.Y = 29;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << char(200);

	c.X = 99;
	c.Y = 29;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << char(188);
	for (i = 1; i < 99; i++)
	{
		c.Y = 0;
		c.X = i;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << char(205);
		if (i == 81)
			cout << char(203);

		c.Y = 29;
		c.X = i;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << char(205);
		if (i == 81)
			cout << char(202);
	}

	for (j = 1; j < 29; j++)
	{
		c.Y = j;
		c.X = 0;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << char(186);

		c.Y = j;
		c.X = 81;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << char(186);

		c.Y = j;
		c.X = 99;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << char(186);
	}
	c.X = 83;
	c.Y = 3;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "Symulacja v0.1";
	c.X = 83;
	c.Y = 6;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "Liczba samic:";
	c.X = 97;
	c.Y = 6;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << K.liczbaF;
	c.X = 83;
	c.Y = 8;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "Liczba samcow:";
	c.X = 97;
	c.Y = 8;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << K.liczbaM;
	c.X = 83;
	c.Y = 10;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "W sumie:";
	c.X = 97;
	c.Y = 10;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << K.liczbak;
}
void Plansza::UmiescKrolika()
{
	int i = 0, j = 0;
	srand(time(NULL));
	i = (rand() % 80) + 1;
	j = (rand() % 28) + 1;
	while (Zbior[i][j] != nullptr)
	{
		j = (rand() % 28) + 1;
		i = (rand() % 80) + 1;
	}
	Zbior[i][j] = K.Head;
}
void Plansza::WrysojKrolika()
{
	COORD c;
	for (int i = 0; i < 80; i++)
	{
		for (int j = 0; j < 28; j++)
		{
			if (Zbior[i][j] != nullptr)
			{

				c.X = i + 1;
				c.Y = j + 1;
				SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
				cout << char(64);
			}
		}
	}
}
void Plansza::Przerysowanie()
{
	for (int i = 0; i < 80; i++)
	{
		for (int j = 0; j < 28; j++)
		{
			if (Zbior[i][j] != nullptr && Zbior[i][j]->lb == obrot)
			{
				krok = Zbior[i][j]->GdzieIsc();
				if (krok == 1)
				{
					if (i == 1 && j == 1)
					{
						Zbior[79][27] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else if (i == 1 && j != 1)
					{
						Zbior[79][j - 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else
					{
						Zbior[i - 1][j - 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
				}
				else if (Zbior[i][j]->krok == 2)
				{
					if (j == 1)
					{
						Zbior[i][27] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else
					{
						Zbior[i][j - 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
				}
				else if (Zbior[i][j]->krok == 3)
				{
					if (i == 79 && j == 1)
					{
						Zbior[1][27] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else if (i == 1 && j != 1)
					{
						Zbior[79][j - 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else
					{
						Zbior[i + 1][j - 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
				}
				else if (Zbior[i][j]->krok == 4)
				{

					if (i == 1)
					{
						Zbior[79][j] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else
					{
						Zbior[i - 1][j] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
				}
				else if (Zbior[i][j]->krok == 5)
				{
					continue;
				}
				else if (Zbior[i][j]->krok == 6)
				{
					if (i == 79)
					{
						Zbior[1][j] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else
					{
						Zbior[i + 1][j] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
				}
				else if (Zbior[i][j]->krok == 7)
				{
					if (i == 1 && j == 27)
					{
						Zbior[79][1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else if (i == 1 && j != 27)
					{
						Zbior[79][j + 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else
					{
						Zbior[i + 1][j + 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
				}
				else if (Zbior[i][j]->krok == 8)
				{
					if (j == 27)
					{
						Zbior[i][1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else
					{
						Zbior[i][j + 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
				}
				else if (Zbior[i][j]->krok == 9)
				{
					if (i == 79 && j == 27)
					{
						Zbior[1][1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else if (i == 1 && j != 1)
					{
						Zbior[79][j + 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}
					else
					{
						Zbior[i + 1][j + 1] = Zbior[i][j];
						Zbior[i][j] = nullptr;
					}

				}
				else
				{
					COORD c;
					c.X = 86;
					c.Y = 20;
					SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
					cout << "Bl�d krokow";
				}

				COORD c;
				c.X = i + 1;
				c.Y = j + 1;
				SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
				cout << char(' ');


			}
		}
	}
}
Plansza::Plansza()
{
	UstawieniaPoczatkowe();
	TworzP();
	for (int i = 0; i < 80; i++)
	{
		for (int j = 0; j < 28; j++)
		{
			Zbior[i][j] = nullptr;
		}

	}
	for (int i = 0; i < K.liczbak; i++)
	{
		K.DodajNaPoczatek(K.Head, K.Tail, i);
		UmiescKrolika();
	}
	WrysojKrolika();
	for (int i = 0; i < 6; i++){
		COORD c;
		c.Y = 12 + i;
		c.X = 86;
		K.Current = K.Head;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << K.Current->id;
		K.Current = K.Current->Next;
	}
	while (Exit.koniec == false)
	{

		Przerysowanie();
		WrysojKrolika();
		obrot++;
		Sleep(200);
	}
	Sleep(2000);


}


Plansza::~Plansza()
{
}
