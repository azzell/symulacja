#pragma once
#include "Stado.h"
#include <windows.h>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Sterowanie.h"
class Plansza
{
public:
	int obrot=0;
	int wys = 30;
	int szer = 100;
	int krok=0;
	Krolik * Zbior[80][28];
	Stado K;
	Sterowanie Exit;
	char plansza[100][30];/*80/28*/
	void TworzP();
	void Przerysowanie();
	void UmiescKrolika();
	void WrysojKrolika();
	void Czyszczenie();
	void AkualizacjaDanych(int liczbaF, int liczbaM);
	void UstawieniaPoczatkowe();
	Plansza();
	~Plansza();
};

